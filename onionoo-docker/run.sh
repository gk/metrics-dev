#!/bin/bash

cd /srv/onionoo.torproject.org/onionoo
java -Xmx4g -Dsun.net.client.defaultConnectTimeout=60000 -Dsun.net.client.defaultReadTimeout=60000 -DLOGBASE=logs -cp onionoo-latest.jar org.torproject.metrics.onionoo.cron.Main &
java -Xmx2g -DLOGBASE=web-logs -jar onionoo-latest.war 
