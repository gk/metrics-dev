# Metrics Dev

Collection of docker setup to develop on metrics projects

To start developing on a project cd to the project folder and:
```
$ docker build . -t <tag>
$ docker container run -d --network=host --name <name> -it <tag>:latest
```

You can now access the service at 127.0.0.1:8080 on your local machine.

You can also ssh into the container with:
```
$ docker exec -it <name> /bin/bash
```

The docker image by default checkout the `main` branch. So if you are testing or working on a different branch, you have to make sure to change that before building the image. To do that you have to edit the `Dockerfile` , ex: https://gitlab.torproject.org/tpo/network-health/metrics/metrics-dev/-/blob/main/onionoo-docker/Dockerfile#L19
